FROM registry.gitlab.com/infograb/docker/base-images/python:3.10.6-alpine
WORKDIR /usr/src/app
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
COPY . .
RUN python3 -m pip install -r requirements.txt
EXPOSE 5000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:5000"]
